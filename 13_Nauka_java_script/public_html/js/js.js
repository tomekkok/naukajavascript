
(function($) {
    
    //zad 3a
    function Person(name, surname, age, pesel) {
        this.name = name;
        this.surnamne = surname;
        this.age = parseInt(age);
        this.pesel = pesel;
        this.getFullName = function() {
            return this.name + ' ' + this.surnamne;
        };
        this.gender = function() {
            return parseInt(this.pesel.charAt(9)) % 2 === 0?"Kobieta":"Mężczyzna";
            //return this.name.charAt(this.name - 1)=== 'a'?"Kobieta":"Mężczyzna";
        };
        this.getBornYear = function() {
            return new Date().getFullYear() - this.age;
        };
        this.getAsTr = function() {
            return $('<tr/>')
                    .append($('<td/>').text(this.name))
                    .append($('<td/>').text(this.surnamne))
                    .append($('<td/>').text(this.age))
                    .append($('<td/>').text(this.pesel))
                    .append("<td><button>usuń</button></td>")
                    .appendTo('tBody');
        };
    }
    
    
    
    
    
    //zadanie 2
    $('tbody td').filter(function(){
        return $(this).find('button').length === 0;
    }).click(function () {
       var $td = $(this),
           width = $td.css('width');
       
       //zad 2d
        $td.css('width', width);
       
       var value = $(this).text().trim(),
       $input = $('<input/>')
       .val(value)
       .css('width','100%');
       $(this).text('').append($input);
       //zadanie 2b
       $input.select();
       //zadanie 2c
       $input.blur(function() {
           $td.text($(this).val());
           $td.css('width', ''); //tu szerokość jest automatyczna
       // moze być teź $input.on('blur', function.... 
       });
       
        
        
    });
    
    
    
    //zadanie4
//    function sum(a,b) {
//        
//        if (arguments.length === 2) {
//            return a + b;
//        }
//        return function(x) {
//            return a + x;
//        };
//        
//    }
//    console.log(sum(4)(5));
//    console.log(sum(4,5));

    //zadanie2
    function isPrime(number) {
        if (number === 0 || number === 1) {
            return false;
        }
        for (var i = 2; i < number; i++) {
            if (number % i === 0) {
                return false;
            }
        }
        return true;
    }
    function primes(n) {
        var resultArray = [];
        
        for (var i = 2; i <= n; i++) {
            if (isPrime(i)) {
                resultArray.push(i);
            }
        }
        return resultArray;
    }
    
    //zadanie 3
    function fastPrimes(n) {
        var resultArray =[],//tu wpisujemy znalezione liczby pierwsze
            tempArray = new Array(n + 1);//tworzymy pusta arraylice n-elementowa
        for (var i = 2; i <= Math.sqrt(n); i++) {
            if (tempArray[i]) {//jesli ta liczba jest juz wpisana do
                continue;//arraylicy, to idz do nastepnej iteracji
            }
            for (var j = 2*i; j <= n; j += i) {
                tempArray[j] = true;//zaznaczamy wszystkie liczby
            }//zlozone w arraylicy tempArray
        }
        for (var i = 2; i <= n; i++) {
            if (!tempArray[i]) {//jezeli liczba w tempArray nie zostala
                resultArray.push(i);//zaznaczona, to dodajemy ja do
            }//resultArray
        }
        return resultArray;
    }
    
    //var now = new Date().getTime();
    //console.log(fastPrimes(10000000));
    //console.log(new Date().getTime() - now);
    //now = new Date().getTime();
    //console.log(primes(100000));
    //console.log(new Date().getTime() - now);
    
    //zadanie1
    Array.prototype.quickSort = function() {
        if (this.length < 2) {
            return this;
        }
        var pivot = this[0],
            left = [],
            right = [];
        for (var i = 1; i < this.length; i++) {
            if (this[i] < pivot) {
                left.push(this[i]);
            } else {
                right.push(this[i]);
            }
        }
        return left.quickSort().concat(pivot, right.quickSort());
    };
    //console.log([6,3,7,8,1,0,2,-3,10,15,11].quickSort());
    
    //zadanie6
    
    //funcka wyłącza przycisk, jeżeli znajdzie elementy .has-error
    function refreshButton() {
        
//        var $button = $('form button');
//        
//        if ($('.has-error').length === 0) {
//            $button.attr('disabled', false);
//        } else {
//            $button.attr('disabled', true);
//        }
        $('form button')
        .attr('disabled', $('.has-error').length > 0);
    }
    
    
    
    $(document).on('click', 'tbody button', function() {
        if (confirm('Are you sure?')) {
        $(this).closest('tr').fadeOut(500, function() {
            $(this).remove();
        });
        }
    });
    
    $('th').click(function() {
        var index = $(this).index();
        $('tbody tr').sort(function(tr1,tr2) {
            return $(tr1).find('td').eq(index).text() > $(tr2).find('td').eq(index).text() ? 1 : -1;
        }).detach().appendTo('tbody');
    });
    
    //dodawanie wierszy
    $('form').submit(function(e) {
        
        e.preventDefault();
//        $('<tr/>')
//                .append($('<td/>').text($('#name').val()))
//                .append($('<td/>').text($('#surname').val()))
//                .append($('<td/>').text($('#age').val()))
//                .append($('<td/>').text($('#pesel').val()))
//                .append("<td><button>usuń</button></td>")
        //        .appendTo('tBody');
        
        var person = new Person($('#name').val()
                                ,$('#surname').val()
                                ,$('#age').val()
                                ,$('#pesel').val());
        
        person.getAsTr().appendTo('tBody');
                                

        
                $('input').val('');
                //$('form')[0].reset(); inna możliwość resetowania formularza
                $('input').change();
        
    });
    
    //oznacz wszystkie pola jako nieprawidłowe
    $('.form-group').addClass('has-error');
    
    //zablokuj przycisk
    $('form button').attr('disabled', true);
    
    
    function isCorrectPesel(pesel) {
        var reg = /^[0-9]{11}$/;
        if (!reg.test(pesel)) {
            return false;
        } else {
            var dig = ("" + pesel).split("");
            var control = (1 * parseInt(dig[0]) + 3 * parseInt(dig[1]) + 7 * parseInt(dig[2]) + 9 * parseInt(dig[3]) + 1 * parseInt(dig[4]) + 3 * parseInt(dig[5]) + 7 * parseInt(dig[6]) + 9 * parseInt(dig[7]) + 1 * parseInt(dig[8]) + 3 * parseInt(dig[9])) % 10;
            if (control === 0)
                control = 10;
            control = 10 - control;
            return parseInt(dig[10]) === control;
        }
    }
    
    function isCorrectAge(age) {
        if (! /^\d+$/.test(age)) {
            return false;
        } 
        
        return !(age>120 || age<1);
        //wykrzyknik mówi kiedy wiek jest prawidłowy - nawias kiedy jest nieprawidłowy
    }
    
    function isCorrect($input) {
        
        var value = $input.val();
        
        switch ($input.attr("id")){
            case 'pesel':
                return isCorrectPesel(value);
                break;
            case 'age' :
                return isCorrectAge(value);
                break;
            case 'name':
            case 'surname':
                return value.length > 0;
                break;
                
        }              
    }           
    
    $('input').change(function(){
        
        var $formGroup = $(this).closest('.form-group');
        
        if (isCorrect($(this))) {
            $formGroup.removeClass('has-error');
        } else {
            $formGroup.addClass('has-error');
        }
        
        refreshButton();
        
    });
    
    
var janNowak = new Person('jan', "nowak", '12', '12343234232');
console.log(janNowak);
console.log(janNowak.getFullName());
console.log(janNowak.gender());
console.log(janNowak.getBornYear());
console.log(janNowak.getAsTr().html());
})(jQuery);
