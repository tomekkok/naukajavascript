/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//A positive integer D is a factor of a positive integer N if there exists an integer M such that N = D * M.
//
//For example, 6 is a factor of 24, because M = 4 satisfies the above condition (24 = 6 * 4).
//
//Write a function:
//
//    function solution(N);
//
//that, given a positive integer N, returns the number of its factors.
//
//For example, given N = 24, the function should return 8, because 24 has 8 factors, namely 1, 2, 3, 4, 6, 8, 12, 24. There are no other factors of 24.
//Assume that:
//        N is an integer within the range [1..2,147,483,647].
//Complexity:
//        expected worst-case time complexity is O(sqrt(N));
//        expected worst-case space complexity is O(1).


//function solution(N) {
//    
//  var i = 1,
//    result = 0;
//
//    while (i < Math.sqrt(N)) {
//    if (N % i === 0) {
//        result += 2;
//    }
//    i++;
//}
//if (Math.pow(i, 2) === N) {
//    result += 1;
//}
//return result;
//}
//
//
//console.log('for 24');
//console.log(solution(24));
//console.log('for 50');
//console.log(solution(50));
//console.log('for 100');
//console.log(solution(100));
//console.log('for 64');
//console.log(solution(64));
//console.log('for 2500000');
//console.log(solution(2500000));
//console.log('for 200000000');
//console.log(solution(200000000));
//console.log('for 25');
//console.log(solution(25));


//function solution(A) {
//   
//    var result = -1;
//   
//    for(var i = 1; i < A.length; i++) {
//
//    var sum1 = 0,
//        sum2 = 0;
//       
//       for (var j=0; j<i; j++) {
//           
//           sum1 += A[j];
//           
//       }
//       
//       for (var k=i; k<A.length; k++) {
//           
//           sum2 += A[k];
//           
//       }
//       
//     var min = Math.abs(sum1 - sum2);
//       
//        if ( result === -1) {
//            
//            result = min;
//    
//        } else {
//            
//            if (min < result) {
//                
//                result = min;
//                
//                
//            }
//            
//        }
//
//    }
//           
//    return result;
//   
//}
//
//console.log(solution([3, 1, 2, 4, 3], 1));
//
//function solution(A) {
//   
//    var result = -1,
//        sumRight = 0,
//        sumLeft = 0;
//   
//    for (var i = 0; i < A.length; i++) {
//        
//        sumRight += A[i];
//       
//       for (var j=0; j<i; j++) {
//           
//           sumLeft += A[j];
//           sumRight -=A[j];
//       }
//       
//       console.log(sumLeft);
//       //console.log(sumRight);
//       
//     var min = Math.abs(sumLeft - sumRight);
//       
//        if ( result === -1) {
//            
//            result = min;
//    
//        } else {
//            
//            if (min < result) {
//                
//                result = min;
//                
//                
//            }
//            
//        }
//
//    }
//    
//    //console.log(sumRight);
//    //return result;
//   
//}
//
//console.log(solution([3, 1, 2, 4, 3]));


