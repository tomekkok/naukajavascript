/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//var obj = {};
//obj.a = 1;
//obj.b = 'Ala ma kota';
//obj.func = function() { alert('func');};


//var obj = {
//    'a' : 1,
//    'b' : 'Ala ma kota',
//    'func' : function() { alert('func'); }  
//};
//
////console.log(obj.b);
//
//
//function createObject(a,b) 
//{
//    var obj = {};
//    obj.a = a;
//    obj.b = b;
//    obj.func = function()
//    {
//        console.log('func');
//    }
//    
//    return obj;
//}


//var MyObj = createObject(1, 'Ala ma kota');
//var MyObj2 = createObject(2, 'Coś takiego');
//
//console.log(MyObj.a);
//console.log(MyObj.b);
//MyObj.a = 10;
//MyObj.b = 'Ola ma psa';
//console.log(MyObj.a);
//console.log(MyObj.b);
//console.log(MyObj2.a);
//console.log(MyObj2.b);

//function MyObject() {}
//MyObject.prototype.sayHi = function()
//{
//    console.log('Witaj świecie');
//};

//var oMyObj = new MyObject();
//oMyObj.sayHi()

//function MyObject2() {}
//MyObject2.prototype.sentence = 'Witaj świecie kolejny raz';
//MyObject2.prototype.sayHi = function()
//{
//    console.log(this.sentence);
//};
//var pMyObj = new MyObject2();
//pMyObj.sayHi();


//function MyObject (msg)
//{
//    if (msg)
//    {
//        this.sentence = msg;
//    }
//}
//
//MyObject.prototype.sentence = 'Witaj świecie';
//MyObject.prototype.sayHi = function()
//{
//    console.log(this.sentence);
//};
//
//var oMyObj_one = new MyObject();
//var oMyObj_two = new MyObject("A ja nigdy nie będę taki jak inni");
//var oMyObj_three = new MyObject();
//oMyObj_three.sentence = "Za to ja to już w ogóle jestem inny";
//oMyObj_one.sayHi();
//oMyObj_two.sayHi();
//oMyObj_three.sayHi();

//String.prototype.oddzielLitery = function()
//{
//    var n = this.length;
//    var str_result = '';
//    for (var i=0; i<n; i++)
//    {
//        str_result += this.charAt(i) + ' ';
//    }
//    return str_result;
//}

//var str = 'a to troche dluzszy przyklad';
//console.log(str.oddzielLitery());
//
//
//console.log("przykładowy string".oddzielLitery());
//
//var sss = new String("kolejny przyklad");
//
//console.log(sss.oddzielLitery());

//var oNapis1 = new String("Mój pierwszy napis");
//var oNapis2 = new String("kolejny tekst");
//
//console.log(oNapis1.oddzielLitery());
//console.log(oNapis2.oddzielLitery());
//
//oNapis1.oddzielLitery = function()
//{
//    return this.toLowerCase(); //zwraca wszystko małymi literami
//}
//console.log(oNapis1.oddzielLitery());
//console.log(oNapis2.oddzielLitery());

//var MyObjConstr = function(a,b)
//{
//    this.func = function() { console.log('My func 1');}
//    this.a = a;
//    this.b = b;
//};
//
//var MyObj = new MyObjConstr(1, 'Ala ma kota');
//MyObj.func();
//console.log(MyObj.a);
//MyObj.a = 10;
//console.log(MyObj.a);

//function creator()
//{
//    var obj = {};
//    obj.func1 = function()
//    {
//        return 5;//tu coś robimy
//    }
//    obj.func2 = function()
//    {
//        return obj.func1() + 2;
//    }
//    return obj;
//};
//
//
//var o = creator();
//o.func1 = function()
//{
//    return 7;
//}
//o.func2();
//
//console.log(o.func1());
//console.log(o.func2());

//
//function creator2() //poprawna implementacja pól prywatnych
//{
//    
//    function f1()
//    {
//        return 5;//tu coś robimy
//    }
//    function f2()
//    {
//        return f1() + 2;
//    }
//    return {
//        func1 : f1,
//        func2 : f2
//    };
//};
//
//var zx = creator2();
//zx.func2 = function()
//{
//    return 7;
//}
//
//console.log(zx.func2());
//
//


//zad2
//Napisz klasę Pracownik, która będzie zawierała 3 pola: imie, nazwisko oraz wiek.
//Następnie zainicjalizuj dwie instancje klasy Pracownik oraz wyświetl 
//tych pracowników (wydrukuj pola instancji obiektu).

function pracownik(imie, nazwisko, wiek) 
{
    this.imie = imie;
    this.nazwisko = nazwisko;
    this.wiek = wiek;
    this.wypisz = function()
    {
        console.log(this.imie + " " + this.nazwisko + " " + this.wiek);
    }
   
};
var prac1 = new pracownik ("Adam", "Kowalski", 18),
    prac2 = new pracownik ("Ala", "Kot", 18),
    prac3 = new pracownik ("Edzio", "Koc", 30);

//prac1.wypisz();
//prac2.wypisz();
//prac3.wypisz();

function pracownik2(imie, nazwisko, wiek) {
   this.imie = imie;
    this.nazwisko = nazwisko;
    this.wiek = wiek; 
}

var prac1 = new pracownik2 ("Adam", "Kowalski", 18),
    prac2 = new pracownik2 ("Ala", "Kot", 18),
    prac3 = new pracownik2 ("Edzio", "Koc", 30);
    
//console.log(prac1);
//console.log(prac2);
//console.log(prac3);


//zad3

function auto(marka, rokProdukcji) {
    
    this.marka = marka;
    this.rokProdukcji = rokProdukcji;
    
}

function czesc(nazwa, cena) {
    this.nazwa = nazwa;
    this.cena = cena;
 
}

auto.prototype = czesc();

var pojazd = new auto ('Audi', 2005);
pojazd.nazwa = "koło";
pojazd.cena = "1500";

//console.log(pojazd);


//zad5


function Punkt(x,y) {
    this.x = x;
    this.y = y;
    this.addX = function() {
        this.x += 1;
    }
    this.addY = function() {
        this.y += 1;
    }
    this.modX = function(a) {
        this.x = a;
    }
    this.modY = function(b) {
        this.y = b;
    }
}



var p1 = new Punkt (50,100);

//p1.addX();
//p1.addY();
//console.log(p1.x);
//console.log(p1.y);
p1.modX(77);

console.log(p1.x);


