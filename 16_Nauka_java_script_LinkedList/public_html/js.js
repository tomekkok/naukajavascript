/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var LinkedList = function() {
 
    this.lastElement = undefined;
    this.firstElement = undefined;
 
    this.size = 0;
 
    // Adds element on the right of the current element
    this.addAtFront = function(val) {
 
        // If current element is undefined
        if (this.firstElement === undefined) {
 
            this.size++;
 
            this.firstElement = {
                _left: undefined,
                _right: undefined,
                value: val
            };
            this.lastElement = this.firstElement;
 
        } else {
            return this.addOnTheLeftSide(this.firstElement, val);
        }
 
        return this.firstElement;
    };
 
    this.addAtBack = function(val) {
 
        if (this.firstElement === undefined) { // in case of empty
            return this.addAtFront(val);
        } else {
            return this.addOnTheRightSide(this.lastElement, val);
        }
    };
 
    this.addOnTheRightSide = function(node, val) {
 
        this.size++;
 
        var newElement = {
            _left: node,
            _right: undefined,
            value: val
        };
 
        if (node._right !== undefined) {
            newElement._right = node._right;
            node._right._left = newElement;
        } else {
            this.lastElement = newElement;
        }
 
        node._right = newElement;
 
        return newElement;
    };
 
    this.addOnTheLeftSide = function(node, val) {
 
        this.size++;
 
        var newElement = {
            _left: undefined,
            _right: node,
            value: val
        };
 
        if (node._left !== undefined) {
            newElement._left = node._left;
            node._left._right = newElement;
        } else {
            this.firstElement = newElement;
        }
 
        node._left = newElement;
 
        return newElement;
    };
 
    this.removeFirstElement = function() {
 
        this.size--;
 
        if (this.firstElement === this.lastElement) {
            this.firstElement = undefined;
            this.lastElement = undefined;
        }
        else {
            this.firstElement._right._left = undefined;
            this.firstElement = this.firstElement._right;
        }
 
    };
 
    this.removeLastElement = function() {
 
        this.size--;
 
        if (this.firstElement === this.lastElement) {
            this.firstElement = undefined;
            this.lastElement = undefined;
        }
        else {
            this.lastElement._left._right = undefined;
            this.lastElement = this.lastElement._left;
        }
    };
 
    this.verify = function() {
 
        var counter = 0;
 
        var temp = this.firstElement;
        while(temp !== undefined) {
            temp = temp._right;
            counter++;
        }
 
        if (counter !== this.size) {
            return false;
        }
 
        counter = 0;
        temp = this.lastElement;
        while(temp !== undefined) {
            temp = temp._left;
            counter++;
        }
 
        if (counter !== this.size) {
            return false;
        }
 
        return true;
    };
 
    this.printAllElements = function() {
 
        var result = "";
        var elem = this.firstElement;
        while (elem !== undefined) {
 
            result += (elem.value + " ");
            elem = elem._right;
        }
 
        console.log(result);
    };
 
};
 
 


//var list = new LinkedList();
//
//list.addAtFront(1);
//list.addAtFront(2);
//list.addAtFront(3);
//
//var elem = list.firstElement._right;
//list.addOnTheRightSide(elem, 4);
//list.addOnTheRightSide(elem, 5);
//
//elem = elem._right;
//list.addOnTheRightSide(elem, 10);
//
//list.addAtFront(11);
//
//list.addAtBack(21);
//list.addAtBack(22);
//var elem2 = list.addAtBack(23);
//
//list.addOnTheLeftSide(elem2, 30);
//list.addOnTheLeftSide(elem2, 31);
//
//var back = list.addAtBack(60);
//var front = list.addAtFront(80);
//
//list.addOnTheLeftSide(front, 81);
//list.addOnTheRightSide(back, 61);
//
//list.printAllElements();
//// 81 80 11 3 2 5 10 4 1 21 22 30 31 23 60 61
//
//list.removeFirstElement();
//list.removeLastElement();
//
//list.printAllElements();
//// 80 11 3 2 5 10 4 1 21 22 30 31 23 60
//
//console.log('is correct? ' + list.verify());



function solution(array) {

    var queue = new LinkedList(),
            last;

    for (i = 0; i < array.length; i++) {

        if (array[i] === 0) {

            queue.removeFirstElement();

        }

        if (array[i] === 10) {

            last = queue.addAtBack(i);

        }

        if (array[i] > -6 && array[i] < 0) {

            for (j = 0; j < array[j]; j++) {
                
                
                last = last._left;
            }

            last = queue.addOnTheLeftSide(last, i)._left;

        }

        if (array[i] < 6 && array[i] > 0) {
            for (j = 0; j < array[j]; j++) {
                last = last._right;
            }
            last = queue.addOnTheRightSide(last, i)._right;
        }

    }

    queue.printAllElements();

}

solution([10, 10, 10, 10, 10]);

solution([10, 10, 10, 10, 10, -1, -2, -2, -2, -5, -5, 4, 2, 5, -1, -1, 0, 0, 0, 10, 10, 10]);