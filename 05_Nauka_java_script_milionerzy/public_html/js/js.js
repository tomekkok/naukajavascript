/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tab = [{"pytanie": 'Jaki mamy dziś dzień?',
        "odpA": 'Poniedziałek',
        "odpB": 'Wtorek',
        "odpC": 'Środa',
        'odpD': 'Czwartek',
        "pop": 'Wtorek'
    },
    {"pytanie": "Jaka jest pora roku?",
        "odpA": "Zima",
        "odpB": "Wiosna",
        "odpC": "Lato",
        'odpD': "Jesień",
        "pop": "Zima"
    }];

licznik = 0;

uzupelnij(tab[licznik]);

//tablica asocjacyjna
var odp = document.getElementsByTagName('li');
popr_odp == 'null';

function uzupelnij(z) {

    var pyt = document.getElementById('pytanie'),
            odpA = document.getElementById('odpA'),
            odpB = document.getElementById('odpB'),
            odpC = document.getElementById('odpC'),
            odpD = document.getElementById('odpD');
    pyt.innerHTML = z['pytanie'];
    odpA.innerHTML = z['odpA'];
    odpB.innerHTML = z['odpB'];
    odpC.innerHTML = z['odpC'];
    odpD.innerHTML = z['odpD'];
    
    

    
    odpA.removeEventListener();
    odpB.removeEventListener();
    odpC.removeEventListener();
    odpD.removeEventListener();
    
    popr_odp = z['pop'];
    
    odpA.addEventListener('click', correct);
    odpB.addEventListener('click', correct);
    odpC.addEventListener('click', correct);
    odpD.addEventListener('click', correct);
}

function  correct(e) {
    if (e.target.innerHTML === popr_odp) {
        hurra();
        if (tab.length - 1 > licznik)
            uzupelnij(tab[++licznik]);
        else {
            wygryw();
        }
    } else {
        przegryw();
    }
};

//console.log(popr_odp);

function przegryw() {

    //alert('192');
    var el = document.getElementById('przegryw');
    el.style.display = 'block';
}

function wygryw() {
    var el = document.getElementById('wygryw');
    //alert("Brawo!");
    setTimeout(function () {
        el.style.display = 'block';
    }, 1500);
}

function hurra() {
    var el = document.getElementById('popOdp');
    el.style.display = 'block';
    setTimeout(function () {
        el.style.display = 'none';
    }, 3000);
}


var polNaPol = document.getElementById('polNaPol'),
        polNaPolUzyte = false; //flaga - pol na pol nie było użyte

polNaPol.addEventListener('click', function (e) { //kliknięcie na przycisk 50/50

    if (polNaPolUzyte === false) {

        var index = null,
            zle = [];
        for (i = 0; i < odp.length; i++) {
            if (odp[i].innerHTML === popr_odp) {
                index = i;
            }
        }
        zle = znajdzZle(index);
        //console.log(znajdzZle(index));
        odp[zle[0]].innerHTML = '&nbsp;';
        odp[zle[1]].innerHTML = '&nbsp;';
        odp[zle[0]].removeEventListener('click',correct);
        odp[zle[1]].removeEventListener('click',correct);

        odp[zle[0]].classlist.add('dwadziescia');

        odp[zle[1]].classlist.add('dwadziescia');
        
        polNaPol.style.background='red';
        polNaPolUzyte = true;  //jeżeli polnapoł kliknięte  -> flaga uzyta
    }
});

function znajdzZle(index) {
    
    // to samo:
    // var indexPlus = (index + 1 > 0) ?0: (index + 1);
    
    var indexPlus, indexMinus;
    if (index + 1 > 3 ) {
        indexPlus = 0;
    } else { 
        indexPlus = index + 1;
    }
    if (index - 1 < 0) {
        indexMinus = 3;
    } else {
        indexMinus = index - 1;
    }
    return [indexMinus, indexPlus];
}


var publ = document.getElementById('publ'),
        publUzyte = false;

publ.addEventListener('click', function (e) { 
    if (publUzyte === false) {
       for (i = 0; i < odp.length; i++) {
            if (odp[i].innerHTML === popr_odp) {
                odp[i].classlist.add('czterdziesci');
            } else {
                odp[i].classlist.add('dwadziescia');
       }
       }
        
        publ.style.background="red";
        publUzyte = true;
    }
    });  