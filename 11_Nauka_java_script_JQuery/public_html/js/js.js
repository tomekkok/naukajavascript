/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




//(function ($) {
// 
// 
// 
//    function getLongestWord(sentence) {
// 
//        var wordsArray = sentence.split(' '),
//                longestWord = wordsArray[0];
// 
//        for (var i = 1; i < wordsArray.length; i++) {
//            if (longestWord.length < wordsArray[i].length) {
//                longestWord = wordsArray[i];
//            }
//        }
// 
//        return longestWord;
// 
//    }
// 
//    console.log(getLongestWord('To jest przykładowy tekst'));
// 
//})(jQuery);

//
//(function ($) {
//  
//    function isPrime(number) {
//
//    if (number < 2) {
//        return false;
//    }
//   
//    for (var i=2; i<number; i++) {
//        if (number % i === 0) {
//            return false;
//        } 
//    }
//    
//    return true;
//    }
// 
//    console.log(isPrime(1));
//    console.log(isPrime(5));
//    console.log(isPrime(50));
//    console.log(isPrime(23));
//    
//})(jQuery);


//(function($) {
//   
//    function generateString(length) {
//       
//        var chars = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm",
//        generatedString = '';
//   
//        for(var i = 0; i < length; i++) {
//           
//            var randomNumber = Math.floor((Math.random() * chars.length));
//           
//           
//            generatedString += chars[randomNumber];
//           
//        }
//       
//        console.log(generatedString);
//    }
//   
//    generateString(100);
//   
//})(jQuery);

//(function($) {
//   
//    function range(first, last) {
//        
//        if (first >= last) {
//            console.error('error');
//        }
//        
//        var resultArray = [];
//        
//        for (var i = first+1; i<last; i++) {
//            
//            resultArray.push(i);
//            
//        }
//        
//        return resultArray;
//
//    }
//   
//   console.log(range(5,30));
//   console.log(range(5,5));
//   console.log(range(31,30));
//   
//})(jQuery);


//(function($) {
//   
//    function fibonacci(n) {
//        
//        if (n===1) {
//            return [0];
//        }
//        
//        if (n===2) {
//            return [0, 1];
//        }
//        
//        var previousArray = fibonacci(n-1);
//        previousArray.push(previousArray[previousArray.length-1]+previousArray[previousArray.length-2]);
//        
//        return previousArray;
//
//    }
//   
//console.log(fibonacci(4));
//console.log(fibonacci(22));
//console.log(fibonacci(50));
//console.log(fibonacci(10));
//console.log(fibonacci(1));
//console.log(fibonacci(2));
//   
//})(jQuery);

//(function($) {
//   
//    function randomItem(array) {
//        
//        return array[Math.floor(Math.random()*array.length)];
//        
//    }
//   
//   console.log(randomItem(["Jan", "Ptak", "Auto", "Smok", "Ryba"]));
//   
//})(jQuery);
//
//
//
//(function($) {
//   
//    function stringToArray(text) {
//        
//        return text.split(' ');
//
//    }
//   
//   console.log(stringToArray("kilka pojedynczych slow"));
//   
//})(jQuery);

//Napisz funkcję truncate(), która skróci łańcuch znaków to podanej liczby słów.
//
//(function($) {
//   
//    function truncate(text, wordNumber) {
//        
//        var array = text.split(' ');
//            
//           return array.slice(0, wordNumber).join(' ');
//
//    }
//   
//   console.log(truncate("Napisz funkcję truncate(), która skróci łańcuch znaków to podanej liczby słów.", 6));
//   
//})(jQuery);

//Napisz funkcję allKeys(), która zwróci listę wszystkich właściwości podanego obiektu.
//(function ($) {
// 
//    function allKeys(person) {
//        var keysArray = [];
//       
//        for(var propt in person ) {
//           
//            keysArray.push(propt);
//           
//        }
//       
//        return keysArray;
// 
//    }
//
//    var person = {
//        name: 'Jan',
//        surname: 'Kowalski',
//        age: 35
//    };
// 
//    console.log(allKeys(person));
//})(jQuery);

//Ukryj wszystkie kliknięte nagłówki.
//(function ($) {
// 
//  $('h1').click(function() {
//      $(this).hide(300);
//      
//  });
//  
//})(jQuery);

//Kliknij dwukrotnie na paragraf, by przełączyć kolor jego tła (czerwony – czarny).
//(function ($) {
// 
//  $('p').click(function() {
//      
//      if ($(this).css("background-color")==="rgb(255, 0, 0)") {
//        $(this).css({
//            backgroundColor: "black",
//            color: "white"
//        });
//        
//      } else {
//        $(this).css("background-color", "red");
//      }
//      
//  });
//  
//})(jQuery);

//(function ($) {
// 
//  $('p').click(function() {
//      
//      $(this).toggleClass("active");
//      
//  });
//  
//})(jQuery);

//Przy każdej zmianie rozmiaru okna wypisz na konsolę szerokość okna.
//
//(function ($) {
// 
//  $(window).resize(function() {
//      
//      console.log($(window).width());
//
//  });
//  
//})(jQuery);

//Spraw, by 5 sekund po kliknięciu na prostokąt, prostokąt ten powoli zniknął, a następnie od
//razu ponownie się pojawił.
(function ($) {
 
   $('.box').click(function() {
       
       var $this = $(this);
       
      setTimeout(function(){
         
          $this.fadeOut(1500, function(){
             
              $this.show();
             
          });
         
      }, 5000);
       
   });
 
})(jQuery);