/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*1. Stwórz obiekt, który będzie realizował obliczanie pola oraz obwodu prostokąta.
 * 
 */


function Prostokat(a, b) {

    this.a = a;
    this.b = b;
    this.pole = function () {
        return this.a * this.b;
    };
    this.obwod = function () {
        return (2 * this.a) + (2 * this.b);
    };
}
;

//var a = new Prostokat(2,4);
//console.log(a.pole());
//console.log(a.obwod());

/*zad2
 * Zmodyfikuj powyższy obiekt, aby obliczał także pole / obwód trójkąta, w zależności 
 * od podania trzeciego parametru.
 */

function Liczenie(a, b, c) {
    this.a = parseInt(a, 10);
    this.b = parseInt(b, 10);
    this.c = undefined;

    if (c) {
        this.c = parseInt(c, 10);
        var tmp = [this.a, this.b, this.c];
        tmpA = null;
        tmpB = null;
        tmpA = Math.max(tmp[0], tmp[1], tmp[2]);
        tmp.splice((tmp.indexOf(tmpA)), 1);
        tmpB = tmp[0] + tmp[1];
        if (tmpA >= tmpB) {
            console.log('To nie jest trójkąt!');
            return;
        }

    }
    ;

    this.pole = function () {
        if (this.c) {

            //console.log(this.a);
            //console.log(this.b);
            //console.log(this.c);

            return (Math.sqrt((this.a + this.b + this.c) *
                    (this.a + this.b - this.c) *
                    (this.a - this.b + this.c) *
                    (-this.a + this.b + this.c))) / 4;
        } else {
            return this.a * this.b;
        }

    };
    this.obwod = function () {

        if (this.c) {
            return this.a + this.b + this.c;
        } else {
            return (2 * this.a) + (2 * this.b);
        }
    };
}
;

//var b = new Liczenie(5,6,10);
//console.log(b.pole());
//console.log(b.obwod());


//function Liczenie2(a, b, c) { //gorszy sposób (wbrew konwencji)
//    this.a = parseInt(a,10);
//    this.b = parseInt(b,10);
//
//    
//    if (c) { 
//        this.c = parseInt(c);
//        this.pole = function() { 
//               return (Math.sqrt((this.a + this.b + this.c)*
//                            (this.a + this.b - this.c)*
//                            (this.a - this.b + this.c)*
//                            (-this.a + this.b + this.c)))/4;
//                };
//        this.obwod = function(){
//                return this.a + this.b + this.c; 
//            };
//    } else {
//        this.pole = function() {
//            return this.a* this.b;
//        };
//        this.obwod = function() {
//        return (2*this.a)+(2*this.b);
//    };
//    }
//};

//var c = new Liczenie2(5,6,10);
//console.log(c.pole());
//console.log(c.obwod());


//function Liczenie3(a, b, c) {
//    this.a = parseInt(a,10);
//    this.b = parseInt(b,10);
//    this.c = undefined;
//    
//    if (c) { 
//        this.c = parseInt(c,10);
//    };
//    
//    this.CzyTrojkat = function() {
//        
//        if (this.c) {
//            return ((this.a + this.b) > this.c) &&
//                    ((this.a + this.c) > this.b) &&
//                    ((this.b + this.c) > this.a); 
//        }
//        return false;
//    }
//    
//    this.pole = function() {
//       if (this.c) {
//
//           return (Math.sqrt((this.a + this.b + this.c)*
//                            (this.a + this.b - this.c)*
//                            (this.a - this.b + this.c)*
//                            (-this.a + this.b + this.c)))/4;
//       } else {
//           return this.a* this.b;
//       }
//       
//    };
//    this.obwod = function() {
//        
//        if (this.c) {
//            return this.a + this.b + this.c;   
//        } else {
//            return (2*this.a)+(2*this.b);
//        }
//    };   
//};
//
//var d = new Liczenie3(2,6,10);
//console.log(d.CzyTrojkat());
//console.log(d.pole());
//console.log(d.obwod());


/* zadanie3
 * Napiszmy obiekt, dostarczający metody walidujące formularz. Niech metody sprawdzają
 * wypełnioną długość pól formularza, czy dane pole zawiera litery, cyfry oraz poprawność adresu
 * email.
 */


function weryfikuj() {

    this.inputy = document.querySelectorAll('input[type=text]');

    this.czyPuste = function () {
        var puste = false;
        for (i = 0; i < this.inputy.length; i++) {
            if (this.inputy[i].value == "") {
                puste = true;
            }
        }
        return puste;
    }
    this.czyLitery = function () {
        var pattern = /[A-Za-z]/; //wyrażenie regularne (badanie ciagów znaków zostałt podane dwa zakresy litery A-Z i a-z
        // if(pattern.test(this.inputy[0].value) && (pattern.test(this.inputy[i].value)
        for (i = 0; i <= 1; i++) {
            var czyZawiera = false;
            if (pattern.test(this.inputy[i].value)) {
                czyZawiera = true;
            }
        }
        return czyZawiera;
    }
    this.czyLiczby = function () {
        var pattern2 = /[0-9]/;
        var czyZawiera = false;
        if (pattern2.test(this.inputy[2].value)) {
            czyZawiera = true;
        }
        return czyZawiera;
    }
    this.czyMail = function () {
        var pattern3 = /@/;
        var czyZawiera = false;
        if (pattern3.test(this.inputy[3].value)) {
            czyZawiera = true;
        }
        return czyZawiera;
    }
}

var but = document.getElementById('sprawdz');
but.addEventListener('click', function (e) {

    var input = new weryfikuj();
    console.log('czy w formularzu są puste pola?');
    console.log(input.czyPuste());
    console.log('czy pole imię i nazwisko zawiera litery?');
    console.log(input.czyLitery());
    console.log('czy pole wiek zawiera liczby?');
    console.log(input.czyLiczby());
    console.log('czy pole mail jest poprawne?');
    console.log(input.czyMail());

});
