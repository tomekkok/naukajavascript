/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//
//Napisz funkcję primaAprilis(yearFrom, yearTo), która w zadanym
//przedziale zwróci tablicę lat, w których 1 kwietnia przypada na wtorek.
//
//(function ($) {
//
//
//
//    function primaAprilis(yearFrom, yearTo) {
//
//        var yearsArray = [];
//
//        for (var year = yearFrom; year <= yearTo; year++) {
//
//            var pa = new Date(year, 3, 1);
//
//            if (pa.getDay() === 2) {
//                yearsArray.push(year);
//            }
//        }
//
//        return yearsArray;
//
//    }
//
//    console.log(primaAprilis(1900, 2016));
//
//})(jQuery);



//Napisz program, który losuje liczbę od 1 do 5, prosi użytkownika o
//zgadnięcie liczby, a następnie informuje, czy użytkownik odgadł liczbę.


//(function ($) {
//
//
//
////    function randomNumber() {
////    }
//
//    var randomNumber = Math.floor((Math.random() * 5) +1);
//
//    var userAnswer = parseInt(prompt('Podaj liczbę'));
//    
//    if (userAnswer === randomNumber) {
//           
//         alert('Ekstra!/nJesteś geniuszem');
//
//    } else {
//        alert ('Liczba wynosiła ' + randomNumber);
//    }
//
//
//
//})(jQuery);

//Napisz funkcję reverseNumber(), która odwróci podaną liczbę (1234 =>
//4321).


//(function ($) {
//
//
//
//    function reverseNumber(x) {
//
//        return parseInt(x.toString().split('').reverse().join(''));
//
//    }
//
//    //aleternatywny sposob na zrobienie z liczby łąncucha znakow
//    // string = '' + number;
//
//    console.log(reverseNumber(123456));
//
//
//})(jQuery);


//Napisz funkcję alphabetOrder(), która zwróci podany łańcuch znaków z
//literami uporządkowanymi według alfabetu („wyraz” - „arwyz”).

//(function ($) {
//
//
//
//    function alphabetOrder(word) {
//
//        return word.split('').sort(function (a, b) {
//
//            return a.localeCompare(b);
//
//        }).join('');
//
//    }
//
//    console.log(alphabetOrder('w@a1yraz'));
//
//})(jQuery);

//Napisz funkcję firstUppercase(), która przyjmie zdanie i zamieni pierwszą
//literę każdego słowa na wielką.
//
//((function ($) {
// 
//    function firstUppercase(text) {
// 
//        var array = text.split(' ');
// 
//        for (var i = 0; i < array.length; i++) {
//            array[i] = array[i].charAt(0).toUpperCase()
//                    + array[i].substring(1);
//        }
//       
//        return array.join(' ');
//    }
// 
// 
//    console.log(firstUppercase('to jest przykładowe zdanie'));
// 
//})(jQuery);

//Napisz funkcję vowelCount(), która zwróci liczbę samogłosek w 
//podanym łańcuchu znaków.

//(function ($) {
//
//    function vowelCount(text) {
//
//        var vowels = 'aąeęioóuyAĄEĘIOÓUY'.split(''),
//                vowelsNumber = 0;
//
//        for (var i = 0; i < text.length; i++) {
//
//            if (vowels.indexOf(text.charAt(i)) !== -1) {
//                vowelsNumber++;
//            }
//        }
//
//        return vowelsNumber;
//    }
//
//    console.log(vowelCount('To jest przykładowy tekst'));
//
//})(jQuery);

//Napisz funkcję factors(), która zwróci tablicę wszystkich dzielników 
//podanej liczby.

//(function ($) {
//
//    function factors(n) {
//        var factor = [];
//
//        for (var i = 1; i <= Math.sqrt(n); i++) {
//            if (n % i === 0) {
//                factor.push(i);
//                if (i !== Math.sqrt(n)) {
//                    factor.push(n / i);
//                }
//            }
//        }
//
//        return factor.sort(function (a,b) {
//            if (a>b) {
//                return (a>b);
//            }
//        });
//    }
//    console.log(factors(64));
//    console.log(factors(6));
//    console.log(factors(100));
//
//})(jQuery);

//Napisz funkcję factorial(), która rekurencyjnie zwróci silnię podanej liczby.

//(function ($) {
//
//    function factorial(n) {
//  
//    if (n===1) {
//        return 1;
//    } 
//        return n*factorial(n-1);
//    }
//    
//    }
//
//console.log(factorial(8));
//console.log(factorial(1));
//console.log(factorial(5));
//
//})(jQuery);

// Napisz funkcję timeConvert(), która zamieni liczbę minut na liczbę 
// godzin i minut (200 => 3:20).

//(function ($) {
//
//    function timeConvert(minutes) {
//        
//        var hours = Math.floor(minutes/60),
//            min = minutes%60;
//        
//        return hours + ' : ' + min;
//    }
//
//    console.log(timeConvert(320));
//    console.log(timeConvert(120));
//    console.log(timeConvert(170));
//
//})(jQuery);

// Napisz funkcję protectEmail(), która skróci pierwszy człon adresu (przed
//@) o połowę, by zabezpieczyć się przed ew. spamem.
//(dabrowski@ulicareklamowa.pl => dabr...@ulicareklamowa.pl).

//
//(function ($) {
//
//    String.prototype.halfstring = function () {
//        return this.substring(0, Math.floor(this.length / 2));
//    };
//
//    function protectEmail(email) {
//
//        var mailParts = email.split('@');
//
//        return mailParts[0].halfstring() + '...@' + mailParts[1];
//
//    }
//
//    console.log(protectEmail('alamakota@onet.pl'));
//
//})(jQuery);

//rozważ kod - zmienna b jest globalna - poneiewaz nie ma "var" przed nia
//(function () {
//    var a = b = 5;
//})();
//console.log(a, b);

//Stwórz metodę repeatify(), którą będzie można wykonać na łańcuchu
//znaków. Metoda powinna przyjmować łańcuch znaków oraz liczbę jego
//powtórzeń. Wywołanie repeatify('hey', 3) powinno zwrócić 'heyheyhey'.


//(function ($) {
//
//    function repeatify(text, number) {
//
//        var string = '';
//
//        for (var i = 0; i < number; i++) {
//            string = string + text;
//        }
//
//        return string;
//
//    }
//
//    console.log(repeatify('hey', 3));
//
//})(jQuery);

// zad13
//function test() {
//console.log(a);
//console.log(foo());
//var a = 1;
//function foo() {
//return 2;
//}
//}
//test();

//Stwórz prosty zegar cyfrowy, który będzie się odświeżał co sekundę.

//(function ($) {
//
//    function clock() {
//        
//        var currentDate = new Date(),
//            hours = currentDate.getHours(),
//            min = currentDate.getMinutes(),
//            secs = currentDate.getSeconds();
//    
//        if (min < 10) {
//            min = "0" + min;
//        }
//    
//        if (secs < 10) {
//            secs = "0" + secs;
//        }
//        
//        if (hours < 10) {
//            hours = "0" + hours;
//        }
//        
//        $(".clock").text(hours + ' : ' + min + " : " + secs );
//    }
//
//   setInterval(clock,1000);
//
//})(jQuery);

//z biblioteką moment javascript

//(function ($) {
// 
//    setInterval(function(){
//        //moment.locale('pl');
//        $('.clock').text(moment().format('LTS'));
//       
//    }, 1000);
//})(jQuery);

//Stwórz klasyczny stoper, działający na wciśnięcie spacji.
//
//(function ($) {
// 
//    var $clock = $('.clock');//kazda zmienna ktora oznacza obiekt
//    //jQuery'owy powinna byc zapisywana ze znakiem '$' z przodu
//    var state = 'ready',
//            currentTime,
//            interval;
// 
//    function resetTimer() {
//        $clock.text('0:00:00');
//    }
// 
//    resetTimer();
// 
//    $(document).keyup(function (e) {
//        if (e.keyCode !== 32) {
//            return;
//        }
//        switch (state) {
//            case 'ready':
// 
//                currentTime = new Date().getTime();
//                interval = setInterval(function () {
//                    var milliseconds = new Date().getTime() - currentTime;
//                    $clock.text(timeSplit(milliseconds));
//                }, 50);
//                state = 'running';
//                break;
// 
//            case 'running':
//                var milliseconds = new Date().getTime() - currentTime;
//                clearInterval(interval);
//                $clock.text(timeSplit(milliseconds));
//                state = 'display';
//                break;
// 
//            case 'display':
//                resetTimer();
//                state = 'ready';
//                break;
//        }
// 
// 
//    });
//    function timeSplit(milliseconds) {
// 
//        var minutes = Math.floor(milliseconds / 60000),
//                seconds = Math.floor((milliseconds / 1000) - minutes * 60),
//                hundredth = Math.floor(milliseconds / 10)
//                - seconds * 100 - minutes * 60 * 100;
// 
//        if (seconds < 10) {
//            seconds = "0" + seconds;
//        }
// 
//        if (hundredth < 10) {
//            hundredth = "0" + hundredth;
//        }
// 
//        return minutes + ":" + seconds + ":" + hundredth;
//    }
// 
// 
//})(jQuery);

//moja funkcja indexOf - dwa różne algorytmy
//bardzo duża ilość kroków tyle ile elementów
//(function ($) {
//
//    Array.prototype.myIndexOf = function (value) {
//
//        for (var i = 0; i < this.length; i++) {
//            if (this[i] === value) {
//                return i;
//            }
//        }
//
//        return -1;
//
//    };
//
//    var array = ['ala', 'ma', 'kota'];
//
//    console.log(array.myIndexOf('ma'));
//
//})(jQuery);

//drugi model
//mniejsza ilość kroków
//wyszukiwanie binarne uporządkowanie tablicy
//uporządkowanie binarne
(function ($) {

    Array.prototype.myIndexOf2 = function (value) {

        var minimumIndex = 0,
                maximumIndex = this.length - 1,
                currentIndx;

        while (minimumIndex <= maximumIndex) {
            currentIndx = Math.floor((minimumIndex + maximumIndex) / 2);
            if (this[currentIndx] === value) {
                return currentIndx;
            }

            if (this[currentIndx] < value) {
                minimumIndex = currentIndx + 1;
            }

            if (this[currentIndx] > value) {
                maximumIndex = currentIndx - 1;
            }
        }
        return -1;
    };

    var array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    console.log(array.myIndexOf2(4));

})(jQuery);


//sortowanie bąbelkowe

(function ($) {

    Array.prototype.bubbleSort = function () {
        var swap,
                swapped;
 
        do {
            swapped = false;
            for (var i = 0; i < this.length - 1; i++) {
 
                if (this[i] > this[i + 1]) {
                    swap = this[i];
                    this[i] = this[i + 1];
                    this[i + 1] = swap;
                    swapped = true;
                }
            }
        } while (swapped);
        return this;
 
    };

    var array = [4, 7, 3, 1, 2, 0, 292, 3214, 234, 2334, 2232, 111, 234, 865, 654, 32, 6, 8, 5];

    console.log(array.bubbleSort());

})(jQuery);